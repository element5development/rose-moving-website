<?php 
/*-------------------------------------------------------------------

Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vel
vestibulum erat. Aliquam iaculis lectus sit amet lorem posuere, at
feugiat arcu imperdiet. Nullam tempor, purus quis aliquam luctus,
purus nulla lobortis diam, eget posuere massa quam a diam. Duis
dignissim velit neque, sed faucibus nulla luctus vitae.  

------------------------------------------------------------------*/
?>

<time datetime="<?php echo get_post_time('c', true); ?>">
	<?php echo get_the_date(); ?>	
</time>

<p class="author-vcard">
	<a href="<?php echo get_author_posts_url(get_the_author_meta('ID')); ?>" rel="author">
		<?php echo get_the_author(); ?>
	</a>
</p>