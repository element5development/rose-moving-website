<?php 
/*-------------------------------------------------------------------

Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vel
vestibulum erat. Aliquam iaculis lectus sit amet lorem posuere, at
feugiat arcu imperdiet. Nullam tempor, purus quis aliquam luctus,
purus nulla lobortis diam, eget posuere massa quam a diam. Duis
dignissim velit neque, sed faucibus nulla luctus vitae.  

------------------------------------------------------------------*/
?>

<section class="copyright">
	<div class="block">

		<div class="block">
			<p>©Copyright <?php echo date('Y'); ?> <?php echo get_bloginfo( 'name' ); ?>. All Rights Reserved. | </p>
			<nav>
				<?php if (has_nav_menu('footer_nav')) :
					wp_nav_menu(['theme_location' => 'footer_nav', 'menu_class' => 'nav']);
				endif; ?>
			</nav>
		</div>
		<p>US DOT #076235</p>

	</div>
</section>