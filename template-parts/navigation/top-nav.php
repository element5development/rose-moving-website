<section class="top-nav">
	<nav>
		<?php if (has_nav_menu('top_nav')) :
			wp_nav_menu(['theme_location' => 'top_nav', 'menu_class' => 'nav']);
		endif; ?>
	</nav>
</section>