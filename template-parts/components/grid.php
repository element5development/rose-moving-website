<section class="grid-items card-container">

<?php if ( get_field('grid_items') == 'services' ) { 
	$args = array(
		'post_type' => array('service'),
		'posts_per_page' => -1,
		'nopaging' => true,
		'ignore_sticky_posts' => true,
	);
	$query = new WP_Query( $args ); ?>
	<?php if ( $query->have_posts() ) : ?>
		<?php while ( $query->have_posts() ) : $query->the_post(); ?>
		<a href="<?php the_permalink(); ?>" class="link-card image-card card">
			<div class="card-image" style="background-image: url('<?php the_field('header_image'); ?>');"></div>
			<h3><?php the_title(); ?></h3>
			<p><?php echo get_excerpt(165); ?></p>
			<div class="button is-ghost">Learn More</div>
		</a>
		<?php endwhile; ?>
	<?php endif; ?>
	<?php wp_reset_query(); ?>
<?php } else if ( get_field('grid_items') == 'resources'  ) { 
	$args = array(
		'post_type' => array('resource'),
		'posts_per_page' => -1,
		'nopaging' => true,
		'ignore_sticky_posts' => true,
	);
	$query = new WP_Query( $args ); ?>
	<?php if ( $query->have_posts() ) : ?>
		<?php while ( $query->have_posts() ) : $query->the_post(); ?>
			<a href="<?php the_permalink(); ?>" class="link-card image-card card">
				<div class="card-image" style="background-image: url('<?php the_field('header_image'); ?>');"></div>
				<h3><?php the_title(); ?></h3>
				<p><?php echo get_excerpt(165); ?></p>
				<div class="button is-ghost">Learn More</div>
			</a>
		<?php endwhile; ?>
	<?php endif; ?>
	<?php wp_reset_query(); ?>
<?php } else { ?>
	<?php if( have_rows('items') ):
		while ( have_rows('items') ) : the_row(); ?>
			<?php $link = get_sub_field('link');  ?>
			<a target="<?php echo $link['target']; ?>" href="<?php echo $link['url']; ?>" class="link-card image-card card">
				<?php $image = get_sub_field('image'); ?>
				<div class="card-image" style="background-image: url('<?php echo $image['url']; ?>');"></div>
				<h3><?php the_sub_field('title'); ?></h3>
				<p><?php the_sub_field('summary'); ?></p>
				<?php $link = get_sub_field('link'); ?>
				<div class="button is-ghost"><?php echo $link['title']; ?></div>
			</a>
		<?php endwhile;
	endif; ?>
<?php } ?>

</section>