<?php /*

Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vel vestibulum erat. Aliquam iaculis lectus
sit amet lorem posuere, at feugiat arcu imperdiet. Nullam tempor, purus quis aliquam luctus, purus nulla 
lobortis diam, eget posuere massa quam a diam. Duis dignissim velit neque, sed faucibus nulla luctus
vitae.  

*/ ?>

<section class="cta-full" style="background-image: url('<?php the_field('cta_background'); ?>');">
	<div class="block">
		<?php the_field('cta_content'); ?>
	</div>
</section>