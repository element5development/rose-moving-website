<?php 
/*-------------------------------------------------------------------

Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vel
vestibulum erat. Aliquam iaculis lectus sit amet lorem posuere, at
feugiat arcu imperdiet. Nullam tempor, purus quis aliquam luctus,
purus nulla lobortis diam, eget posuere massa quam a diam. Duis
dignissim velit neque, sed faucibus nulla luctus vitae.  

------------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/pages/content', 'title'); ?>

<?php if( !empty(get_the_content()) ) { ?>
	<section class="default-contents">
		<?php get_template_part('template-parts/pages/content', 'default'); ?>
	</section>
<?php } ?>

<?php if ( get_field('display_cta') ) {
	get_template_part('template-parts/components/cta_full');
} ?>

<?php if ( get_field('display_testimony_slider') ) {
	get_template_part('template-parts/components/slider_testimony');
} ?>

<?php if ( get_field('display_logo_slider') ) {
	get_template_part('template-parts/components/slider_logos');
} ?>

<?php get_footer(); ?>