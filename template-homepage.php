<?php 
/*-------------------------------------------------------------------
    Template Name: Homepage
-------------------------------------------------------------------*/
?>
<?php get_header(); ?>

<section class="home-title page-title title-section">
	<?php if( have_rows('slider') ):
		while ( have_rows('slider') ) : the_row(); ?>
			<?php 
				$background = get_sub_field('background'); 
				$link = get_sub_field('link');
			?>
			<div class="home-slide" style="background-image: url('<?php echo $background['url']; ?>');">
				<div class="block">
					<div class="title-container" style="background-image: url('<?php echo get_template_directory_uri(); ?>/dist/images/title-image.png');">
						<h1><?php the_sub_field('title'); ?></h1>
						<p><?php the_sub_field('summary'); ?></p>
						<a target="<?php echo $link['target']; ?>" href="<?php echo $link['url']; ?>" class="button"><?php echo $link['title']; ?></a>
					</div>
				</div>
			</div>
		<?php endwhile;
	endif; ?>
</section>

<?php if( have_rows('services') ): ?>
	<section class="services card-container">
		<?php while ( have_rows('services') ) : the_row(); ?>
			<div class="icon-card card">
				<?php 
					$linkOne = get_sub_field('link_one'); 
					$linkTwo = get_sub_field('link_two'); 
				?>
				<a target="<?php echo $linkOne['target']; ?>" href="<?php echo $linkOne['url']; ?>"><div class="card-icon" style="background-image: url('<?php the_sub_field('icon'); ?>');"></div></a>
				<a target="<?php echo $linkOne['target']; ?>" href="<?php echo $linkOne['url']; ?>"><h3><?php the_sub_field('title'); ?></h3></a>
				<?php the_sub_field('list_items'); ?>
				<div class="button-block">
					<a target="<?php echo $linkOne['target']; ?>" href="<?php echo $linkOne['url']; ?>" class="button"><?php echo $linkOne['title']; ?></a>
					<a target="<?php echo $linkTwo['target']; ?>" href="<?php echo $linkTwo['url']; ?>" class="button is-ghost"><?php echo $linkTwo['title']; ?></a>
				</div>
			</div>
		<?php endwhile; ?>
	</section>
<?php endif; ?>

<?php if( !empty(get_the_content()) ) { ?>
	<section class="default-contents">
		<?php get_template_part('template-parts/pages/content', 'default'); ?>
	</section>
<?php } ?>

<?php get_template_part('template-parts/components/slider_testimony'); ?>

<?php if( have_rows('tiles') ): ?>
	<section class="tile-container card-container">
		<?php $i = 0; ?>
		<?php while ( have_rows('tiles') ) : the_row(); $i++; ?>

				<?php if( $i == 2 ) { ?>
					<div class="bg-card card">
						<h3><?php the_sub_field('title'); ?></h3>
						<?php echo do_shortcode('[gravityform id="17" title="false" description="false"]'); ?>
					</div>
				<?php } else { ?>
					<?php $link = get_sub_field('link'); ?>
					<a target="<?php echo $link['target']; ?>" href="<?php echo $link['url']; ?>" class="bg-card card" style="background-image: url('<?php the_sub_field('background'); ?>');">
						<h3><?php the_sub_field('title'); ?></h3>
						<div class="button"><?php echo $link['title']; ?></div>
					</a>
				<?php } ?>

		<?php endwhile; ?>
	</section>
<?php endif; ?>

<?php get_template_part('template-parts/components/slider_logos'); ?>

<?php get_footer(); ?>