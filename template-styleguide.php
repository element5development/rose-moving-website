<?php 
/*-------------------------------------------------------------------
    Template Name: Styleguide
-------------------------------------------------------------------*/
?>

<?php get_header(); ?>

<section class="styleguide-title">
	<div class="block">
		<h1><?php echo get_bloginfo( 'name' ); ?></h1>
		<?php if ( get_option('blogdescription') ) : ?>
			<h2><?php echo get_option('blogdescription'); ?></h2>
		<?php endif; ?>
	</div>
</section>

<main class="styleguide-container">
	<div class="brand block">
		<!--  COLORS  -->
		<section class="styleguide-section">
			<h2>Colors palette</h2>
			<div class="colors">
				<p>Primary Colors</p>
				<div class="color">
					<div class="color-swatch"></div>
					<div class="color-details">
						<p class="hex">HEX<span></span></p>
						<p class="rgb">RGB<span></span></p>
						<p class="cmyk">CMYK<span></span></p>
					</div>
				</div>
				<div class="color">
					<div class="color-swatch"></div>
					<div class="color-details">
						<p class="hex">HEX<span></span></p>
						<p class="rgb">RGB<span></span></p>
						<p class="cmyk">CMYK<span></span></p>
					</div>
				</div>
				<div class="color">
					<div class="color-swatch"></div>
					<div class="color-details">
						<p class="hex">HEX<span></span></p>
						<p class="rgb">RGB<span></span></p>
						<p class="cmyk">CMYK<span></span></p>
					</div>
				</div>
				<div class="color">
					<div class="color-swatch"></div>
					<div class="color-details">
						<p class="hex">HEX<span></span></p>
						<p class="rgb">RGB<span></span></p>
						<p class="cmyk">CMYK<span></span></p>
					</div>
				</div>
				<div class="color">
					<div class="color-swatch"></div>
					<div class="color-details">
						<p class="hex">HEX<span></span></p>
						<p class="rgb">RGB<span></span></p>
						<p class="cmyk">CMYK<span></span></p>
					</div>
				</div>
			</div>
		</section>
		<!--  FONTS -->
		<section class="styleguide-section">
			<h2>FONTS</h2>
			<div class="fonts">
				<div class="font">
					<div class="block">
						<p class="family"></p>
						<p class="example">Aa</p>
						<p class="weight"></p>
					</div>
				</div>
				<div class="font">
					<div class="block">
						<p class="family"></p>
						<p class="example">Aa</p>
						<p class="weight"></p>
					</div>
				</div>
			</div>
		</section>
		<!--  TYPOGRAPHY -->
		<section class="styleguide-section">
			<h2>TYPOGRAPHY</h2>
			<div class="headings">
				<div class="typography">
					<p>H1</p>
					<h1>This is a headline</h1>
				</div>
				<div class="typography">
					<p>H2</p>
					<h2>This is a headline</h2>
				</div>
				<div class="typography">
					<p>H3</p>
					<h3>This is a headline</h3>
				</div>
				<div class="typography">
					<p>H4</p>
					<h4>This is a headline</h4>
				</div>
				<div class="typography">
					<p>H5</p>
					<h5>This is a headline</h5>
				</div>
				<div class="typography">
					<p>Paragraph</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque sit amet pharetra metus. Sed a purus massa. Cras tempus pharetra quam, nec mattis dui semper et. Nunc sapien arcu, bibendum a euismod eget, facilisis quis nisl.</p>
				</div>
				<div class="typography">
					<p>Legal</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque sit amet pharetra metus. Sed a purus massa. Cras tempus pharetra quam, nec mattis dui semper et. Nunc sapien arcu, bibendum a euismod eget, facilisis quis nisl.</p>
				</div>
			</div>
		</section>
		<!--  LOGOS  -->
		<section class="styleguide-section">
			<h2>Logo</h2>
			<div class="logos">
				<div class="logo">
					<?php $logo_primary = get_field('primary_logo'); ?>
					<a target="_blank" href="<?php echo $logo_primary['url']; ?>">
						<img src="<?php echo $logo_primary['url']; ?>" alt="<?php echo $logo_primary['alt']; ?>" />
					</a>
				</div>
				<?php if( have_rows('logo_variant') ):
					while ( have_rows('logo_variant') ) : the_row(); ?>
						<div class="logo" style="background-color: <?php the_sub_field('background_color'); ?>">
						<?php $logo_variant = get_sub_field('logo'); ?>
						<a target="_blank" href="<?php echo $logo_variant['url']; ?>">
							<img src="<?php echo $logo_variant['url']; ?>" alt="<?php echo $logo_variant['alt']; ?>" />
						</a>
						</div>
					<?php endwhile;
				endif; ?>
			</div>
		</section>
		<!--  ICONS -->
		<?php if( have_rows('icons') ): ?>
			<section class="styleguide-section">
				<h2>ICONS</h2>
				<div class="icons">
					<?php while ( have_rows('icons') ) : the_row(); ?>
						<div class="icon">
							<?php $icon = get_sub_field('icon'); ?>
							<a target="_blank" href="<?php echo $icon['url']; ?>">
								<img src="<?php echo $icon['url']; ?>" alt="<?php echo $icon['alt']; ?>" />
							</a>
						</div>
					<?php endwhile; ?>
				</div>
			</section>
		<?php endif; ?>
		<!--  IMAGERY -->
		<?php if( have_rows('imagery') ): ?>
			<section class="styleguide-section">
				<h2>IMAGERY</h2>
				<div class="images">
					<?php while ( have_rows('imagery') ) : the_row(); ?>
						<?php $image = get_sub_field('image'); ?>
						<a target="_blank" href="<?php echo $image['url']; ?>" class="image" style="background-image: url('<?php echo $image['url']; ?>');"></a>
					<?php endwhile; ?>
				</div>
			</section>
		<?php endif; ?>
	</div>
	<div class="ui-elements block">
		<!--  BUTTONS -->
		<section class="styleguide-section">
			<h2>Buttons</h2>
			<div class="buttons">
				<p>Primary</p>
				<a href="" class="button">button</a>
				<a href="" class="button is-hover">hover</a>
				<a href="" class="button is-active">active</a>
			</div>
			<div class="buttons">
				<p>Secondary</p>
				<a href="" class="button is-secondary">button</a>
				<a href="" class="button is-secondary is-hover">hover</a>
				<a href="" class="button is-secondary is-active">active</a>
			</div>
			<div class="buttons">
				<p>Ghost Buttons</p>
				<a href="" class="button is-ghost">button</a>
				<a href="" class="button is-ghost is-hover">hover</a>
				<a href="" class="button is-ghost is-active">active</a>
			</div>
			<div class="buttons">
				<p>Social Links</p>
				<a target="_blank" href="https://www.facebook.com/" class="social-button facebook">
					<svg viewBox="0 0 50 50" xmlns="http://www.w3.org/2000/svg" fill-rule="evenodd" clip-rule="evenodd" stroke-linejoin="round" stroke-miterlimit="1.414">
						<path d="M32.197 48.32V27.047h7.137l1.07-8.293h-8.207V13.46c0-2.4.664-4.036 4.11-4.036h4.387v-7.42c-.76-.097-3.363-.324-6.395-.324-6.33 0-10.665 3.864-10.665 10.96v6.114h-7.16v8.293h7.16V48.32h8.562z" fill-rule="nonzero"></path>
					</svg>
				</a>
				<a target="_blank" href="https://www.linkedin.com/" class="social-button linkedin">
				<svg viewBox="0 0 50 50" xmlns="http://www.w3.org/2000/svg" fill-rule="evenodd" clip-rule="evenodd" stroke-linejoin="round" stroke-miterlimit="1.414">
					<path d="M43.017 33.175v13.322h-7.723v-12.43c0-3.122-1.117-5.253-3.913-5.253-2.133 0-3.403 1.436-3.962 2.825-.203.496-.256 1.187-.256 1.882v12.975h-7.726s.104-21.052 0-23.233h7.725v3.293c-.014.025-.035.05-.05.075h.05v-.075c1.028-1.58 2.86-3.84 6.963-3.84 5.083 0 8.894 3.322 8.894 10.458zm-31.662-21.11c-2.643 0-4.372 1.733-4.372 4.013 0 2.23 1.68 4.016 4.27 4.016h.05c2.695 0 4.37-1.786 4.37-4.016-.05-2.28-1.675-4.014-4.318-4.014zM7.442 46.496h7.723V23.264H7.442v23.233z" fill-rule="nonzero"></path>
				</svg>
				</a>
				<a target="_blank" href="mailto:email@email.com" class="social-button email">
					<svg viewBox="0 0 50 50" xmlns="http://www.w3.org/2000/svg" fill-rule="evenodd" clip-rule="evenodd" stroke-linejoin="round" stroke-miterlimit="1.414">
						<path d="M1.34 7.918v34.164h47.32V7.918H1.34zM25 21.658l-13.687-9.124H38.74L25 21.658zM5.956 37.466V14.55l17.77 11.882 1.274.796 1.273-.796 17.77-11.83V37.52H5.957v-.054z" fill-rule="nonzero"></path>
					</svg>
				</a>
			</div>
		</section>
		<!-- INPUT FIELDS -->
		<section class="styleguide-section">
			<h2>INPUT FIELDS</h2>
			<?php echo do_shortcode('[gravityform id="5" title="false" description="false"]'); ?>
		</section>
		<!-- LISTS -->
		<section class="styleguide-section">
			<h2>Lists</h2>
			<div class="lists">
				<div class="list">
					<ul>
						<li>Unordered List</li>
						<li>Unordered List</li>
						<li>Unordered List</li>
					</ul>
				</div>
				<div class="list">
					<ol>
						<li>Unordered List</li>
						<li>Unordered List</li>
						<li>Unordered List</li>
					</ol>
				</div>
			</div>
		</section>
		<!-- BLOCKQUOTE -->
		<section class="styleguide-section">
			<h2>Blockquote</h2>
			<div class="quotes">
				<blockquote>
					<p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam eu condimentum ex, non tincidunt enim."</p>
					<p>Quotee</p>
				</blockquote>
			</div>
		</section>
	</div>
</main>

<?php get_footer(); ?>