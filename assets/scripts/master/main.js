var $ = jQuery;

$(document).ready(function() {
  /*-----------------------------------------------------------------
		Form input adding & removing classes
	------------------------------------------------------------------*/
  $('input:not([type=checkbox]):not([type=radio])').focus(function() {
    $(this).addClass('is-activated');
  });
  $('textarea').focus(function() {
    $(this).addClass('is-activated');
  });
  $('select').focus(function() {
    $(this).addClass('is-activated');
  });
  /*-----------------------------------------------------------------
  	OPEN & CLOSE PRIMARY NAV
  ------------------------------------------------------------------*/
  $('#menu').on("click", function() {
    $('.primary-nav-container').toggleClass('open');
    $('.hamburger').toggleClass('open');
  });
  /*-----------------------------------------------------------------
  	OPEN & CLOSE SEARCH FORM
	------------------------------------------------------------------*/
  $('#search').on("click", function() {
    $('.search-icon-contain').toggleClass('open');
    $('.search-container').toggleClass('open');
  });
  /*-----------------------------------------------------------------
  	TESTIMONY SLIDER
  ------------------------------------------------------------------*/
  $('.slider-quotes').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    prevArrow: '<svg viewBox="0 0 72 72"><path d="M36 71C16.7 71 1 55.3 1 36S16.7 1 36 1s35 15.7 35 35-15.7 35-35 35zm0-66.7C18.5 4.3 4.3 18.5 4.3 36S18.5 67.7 36 67.7 67.7 53.5 67.7 36 53.5 4.3 36 4.3z"/><path d="M41 51c-.4 0-.9-.2-1.2-.5L24.5 35.2l15.3-15.3c.7-.7 1.7-.7 2.4 0s.7 1.7 0 2.4l-13 13 13 13c.7.7.7 1.7 0 2.4-.3.1-.8.3-1.2.3z"/></svg>',
    nextArrow: '<svg viewBox="0 0 72 72"><path d="M36 71C16.7 71 1 55.3 1 36S16.7 1 36 1s35 15.7 35 35-15.7 35-35 35zm0-66.7C18.5 4.3 4.3 18.5 4.3 36S18.5 67.7 36 67.7 67.7 53.5 67.7 36 53.5 4.3 36 4.3z"/><path d="M31 52.6c-.4 0-.9-.2-1.2-.5-.7-.7-.7-1.7 0-2.4l13-13-13-13c-.7-.7-.7-1.7 0-2.4s1.7-.7 2.4 0l15.3 15.3-15.3 15.5c-.3.4-.8.5-1.2.5z"/></svg>',
    dots: false,
    focusOnSelect: true,
    fade: true,
    speed: 1000,
    cssEase: 'linear',
    autoplay: true,
    autoplaySpeed: 4000,
  });
  /*-----------------------------------------------------------------
  	LOGO SLIDER
  ------------------------------------------------------------------*/
  $('.slider-logos').slick({
    arrows: false,
    dots: false,
    infinite: true,
    speed: 300,
    slidesToShow: 4,
    slidesToScroll: 4,
    autoplay: true,
    autoplaySpeed: 2000,
    responsive: [{
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      }
    ]
  });
  /*-----------------------------------------------------------------
  	HOME SLIDER
  ------------------------------------------------------------------*/
  $('.home-title').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    prevArrow: '<svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 72 72"><defs><style>.cls-1,.cls-4{fill:none;}.cls-2{opacity:0.19;}.cls-3{clip-path:url(#clip-path);}.cls-4{stroke:#fff;stroke-linecap:round;stroke-miterlimit:10;stroke-width:2px;}</style><clipPath id="clip-path"><path class="cls-1" d="M484-2050h175.89v34.83H484z"/></clipPath></defs><title>arrow-filled-left</title><circle class="cls-2" cx="35.86" cy="35.48" r="34.48"/><circle class="cls-4" cx="35.86" cy="35.48" r="34.48"/><path class="cls-4" d="M41.03 21.69L26.4 36.32l14.63 14.63"/><circle class="cls-4" cx="35.86" cy="35.48" r="34.48"/></svg>',
    nextArrow: '<svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 72 72"><defs><style>.cls-1,.cls-4{fill:none;}.cls-2{opacity:0.19;}.cls-3{clip-path:url(#clip-path);}.cls-4{stroke:#fff;stroke-linecap:round;stroke-miterlimit:10;stroke-width:2px;}</style><clipPath id="clip-path"><path class="cls-1" d="M484-2050h175.89v34.83H484z"/></clipPath></defs><title>arrow-filled</title><circle class="cls-2" cx="35.86" cy="35.48" r="34.48"/><circle class="cls-4" cx="35.86" cy="35.48" r="34.48"/><path class="cls-4" d="M30.69 21.69l14.63 14.63-14.63 14.63"/><circle class="cls-4" cx="35.86" cy="35.48" r="34.48"/></svg>',
    dots: false,
    focusOnSelect: true,
    fade: true,
    speed: 800,
    cssEase: 'linear',
    autoplay: true,
    autoplaySpeed: 5000,
  });
});