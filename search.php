<?php 
/*-------------------------------------------------------------------

Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vel
vestibulum erat. Aliquam iaculis lectus sit amet lorem posuere, at
feugiat arcu imperdiet. Nullam tempor, purus quis aliquam luctus,
purus nulla lobortis diam, eget posuere massa quam a diam. Duis
dignissim velit neque, sed faucibus nulla luctus vitae.  

------------------------------------------------------------------*/
?>

<?php get_header(); ?>

	<?php get_template_part('template-parts/pages/content', 'title'); ?>

	<section class="default-contents">
		<?php if (!have_posts()) : ?>
			<p>Sorry, no results were found</p>
			<?php get_search_form(); ?>
	<?php else: ?>
		<?php 
			global $wp_query;
			$phone = get_field('primary_phone','options');
			$phone = preg_replace('/[^0-9]/', '', $phone);
			$phone = '+1' . $phone;
		?>
		<h2><?php echo $wp_query->found_posts; ?> results found</h2>
		<p>If these results still do not provide you with what your looking for please contact us at <a href="<?php echo $phone; ?>"><?php the_field('primary_phone','options'); ?></a> </p>
	<?php endif; ?>
	</section>

	<section class="search-results wide card-container">
		<?php while (have_posts()) : the_post(); ?>
			<?php get_template_part('template-parts/pages/content', 'search'); ?>
		<?php endwhile; ?>
	</section>
	<div class="navigation default-contents">
		<div class="block"><?php previous_posts_link( '&laquo; Previous Results' ); ?></div>
		<div class="block"><?php next_posts_link( 'Next Results &raquo;', '' ); ?></div>
	</div>

<?php get_footer(); ?>