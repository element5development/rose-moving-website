<?php 
/*-------------------------------------------------------------------

Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vel
vestibulum erat. Aliquam iaculis lectus sit amet lorem posuere, at
feugiat arcu imperdiet. Nullam tempor, purus quis aliquam luctus,
purus nulla lobortis diam, eget posuere massa quam a diam. Duis
dignissim velit neque, sed faucibus nulla luctus vitae.  

------------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/posts/content', 'title'); ?>

<?php if( !empty(get_the_content()) ) { ?>
	<section class="default-contents">
		<?php get_template_part('template-parts/pages/content', 'default'); ?>
	</section>
<?php } ?>

<?php if( have_rows('moving_format') ): ?>
	<section class="wide card-container">
		<?php while ( have_rows('moving_format') ) : the_row(); ?>
			<div class="image-card card">
				<?php $image = get_sub_field('image'); ?>
				<div class="card-image" style="background-image: url('<?php echo $image['url']; ?>');"></div>
				<h3><?php the_sub_field('title'); ?></h3>
				<p><?php the_sub_field('summary'); ?></p>
			</div>
		<?php endwhile; ?>
	</section>
<?php endif; ?>

<?php if( have_rows('packing_options') ): ?>
	<div class="default-contents">
		<h2>Packing Options</h2>
		<p>We know how important your possessions are to you. We'll treat them with the care that we'd treat our own belongings.</p>
	</div>
	<section class="card-container">
		<?php while ( have_rows('packing_options') ) : the_row(); ?>
			<div class="underline-card card">
				<h3><?php the_sub_field('title'); ?></h3>
				<p><?php the_sub_field('summary'); ?></p>
			</div>
		<?php endwhile; ?>
	</section>
<?php endif; ?>

<?php if( get_field('cta_full_content') ): ?>
	<section class="middle-page cta-full" style="background-image: url('<?php the_field('cta_full_background'); ?>');">
		<div class="block">
			<?php the_field('cta_full_content'); ?>
		</div>
	</section>
<?php endif; ?>

<?php if( have_rows('basic_services') ): ?>
	<div class="default-contents">
		<h2>Basic Household Moving Services</h2>
		<p>Because no move is the same, we offer a variety of household moving services to help make your move as easy and stress free as possible.</p>
	</div>
	<section class="card-container">
		<?php while ( have_rows('basic_services') ) : the_row(); ?>
			<div class="underline-card card">
				<h3><?php the_sub_field('title'); ?></h3>
				<p><?php the_sub_field('summary'); ?></p>
			</div>
		<?php endwhile; ?>
	</section>
<?php endif; ?>

<?php if( have_rows('additional_services') ): ?>
	<div class="default-contents">
		<h2>Additonal Household Moving Services</h2>
		<p>If you need special moving services, we’ve got you covered.</p>
	</div>
	<section class="card-container">
		<?php while ( have_rows('additional_services') ) : the_row(); ?>
			<div class="underline-card card">
				<div class="card-icon" style="background-image: url('<?php the_sub_field('icon'); ?>');"></div>
				<h3><?php the_sub_field('title'); ?></h3>
				<p><?php the_sub_field('summary'); ?></p>
			</div>
		<?php endwhile; ?>
	</section>
<?php endif; ?>

<?php if ( get_field('display_cta') ) {
	get_template_part('template-parts/components/cta_full');
} ?>

<?php get_template_part('template-parts/components/slider_testimony'); ?>

<?php get_template_part('template-parts/components/slider_logos'); ?>

<?php get_footer(); ?>