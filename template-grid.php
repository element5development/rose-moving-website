<?php 
/*-------------------------------------------------------------------
		Template Name: Grid
-------------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/pages/content', 'title'); ?>

<?php if( !empty(get_the_content()) ) { ?>
	<section class="default-contents">
		<?php get_template_part('template-parts/pages/content', 'default'); ?>
	</section>
<?php } ?>

<?php if ( is_page(128) ) { //Social Responsibility ?>
	<div class="default-contents">
		<h2>Giving Back</h2>
		<p>Throughout the year we give back to the communities we serve in several different ways.</p>
	</div>
<?php } ?>

<?php if ( get_field('grid_items') == 'full' ) {
	get_template_part('template-parts/components/grid_full'); 
} else {
	get_template_part('template-parts/components/grid'); 
} ?>

<?php if ( get_field('display_cta') ) {
	get_template_part('template-parts/components/cta_full');
} ?>

<?php if ( get_field('display_testimony_slider') ) {
	get_template_part('template-parts/components/slider_testimony');
} ?>

<?php if ( get_field('display_logo_slider') ) {
	get_template_part('template-parts/components/slider_logos');
} ?>

<?php get_footer(); ?>