<?php

/*-----------------------------------------
		MENUS - www.wp-hasty.com
-----------------------------------------*/
function nav_creation() {
	$locations = array(
		'top_nav' => __( 'Top Navigation' ),
		'primary_nav' => __( 'Primary Navigation' ),
		'secondary_nav' => __( 'Secondary Navigation' ),
		'footer_nav' => __( 'Footer Navigation' ),
	);
	register_nav_menus( $locations );

}
add_action( 'init', 'nav_creation' );

/*-----------------------------------------
		MENU ICONS
-----------------------------------------*/
add_filter('wp_nav_menu_objects', 'primary_navigation_objects', 10, 2);

function primary_navigation_objects( $items, $args ) {
  foreach( $items as &$item ) {
    $icon = get_field('icon', $item);

    if( $icon ) {
      $item->title .= '<img src="'.$icon.'">';
    }

  }
  return $items;
}