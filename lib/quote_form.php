<?php
  add_action( 'gform_after_submission_7', 'post_to_third_party', 10, 2 );
  function post_to_third_party( $entry, $form ) {

      $body = array(
        'Email'=> rgar( $entry, '3' ),
        'FirstName'=> rgar( $entry, '1' ),
        'LastName'=> rgar( $entry, '2' ),
        'HomePhone'=> rgar( $entry, '4' ),
        'Address1'=> rgar($entry, '10'),
        'City'=> rgar($entry, '9'),
        'State'=> rgar($entry, '16'),
        'Zip'=> rgar($entry, '11'),
        'DeliveryZip'=> rgar($entry, '12'),
        'MoveDate'=> rgar($entry, '5'),
        'DeliveryDate'=> rgar($entry, '19'),
				'InHouseDate'=> rgar($entry, '20'),
				'HomeType' => rgar($entry, '27'),
        'DwellingType'=> rgar($entry, '13'),
        'LivingRoom'=> rgar($entry, '22.1'),
        'Kitchen'=> rgar($entry, '22.2'),
        'DiningRoom'=> rgar($entry, '22.3'),
        'Office'=> rgar($entry, '22.4'),
        'Comments'=> rgar($entry, '14'),
      );


			if( $body['HomeType'] >= 'C' ) {
				if($body['DwellingType'] >= 3){
					$body['DwellingType'] = 'A3';
				} elseif($body['DwellingType'] == 2){
					$body['DwellingType'] = 'A2';
				} elseif($body['DwellingType'] == 1){
					$body['DwellingType'] = 'A1';
				}
			} else if ( $body['HomeType'] >= 'A' ) {
				if($body['DwellingType'] >= 3){
					$body['DwellingType'] = 'A3';
				} elseif($body['DwellingType'] == 2){
					$body['DwellingType'] = 'A2';
				} elseif($body['DwellingType'] == 1){
					$body['DwellingType'] = 'A1';
				}
			} else {
				if($body['DwellingType'] >= 4){
					$body['DwellingType'] = 'H4';
				} elseif($body['DwellingType'] == 3){
					$body['DwellingType'] = 'H3';
				} elseif($body['DwellingType'] == 2){
					$body['DwellingType'] = 'H2';
				} elseif($body['DwellingType'] == 1){
					$body['DwellingType'] = 'H1';
				}
			}

      $formatReturn = 'xmlRequest=
      <EstimateRequest>
      
      <ReferralCode>ELE20590</ReferralCode>
      
      <PrimaryContact>
      
      <Email>'.strval($body['Email']).'</Email>
      
      <FirstName>'.strval($body['FirstName']).'</FirstName>
      
      <LastName>'.strval($body['LastName']).'</LastName>
      
      <PrimaryPhoneType>H</PrimaryPhoneType>
      
      <PreferredContactTime>E</PreferredContactTime>
      
      <HomePhone>'.strval(preg_replace('/\D+/', '', $body['HomePhone'])).'</HomePhone>
      
      <WorkPhone>
      
      </WorkPhone>
      
      <WorkPhoneExt>
      
      </WorkPhoneExt>
      
      <CellPhone>
      
      </CellPhone>
      
      <FaxPhone>
      
      </FaxPhone>
      
      </PrimaryContact>
      
      <PickupAddress>
      
      <Address1>'.strval($body['Address1']).'</Address1>
      
      <City>'.strval($body['City']).'</City>
      
      <State>'.strval($body['State']).'</State>
      
      <Zip>'.strval($body['Zip']).'</Zip>
      
      </PickupAddress>
      
      <MoveDetails>
      
      <PickupZip>'.strval($body['Zip']).'</PickupZip>
      
      <DeliveryZip>'.strval($body['DeliveryZip']).'</DeliveryZip>
      
      <MoveDate>'.strval($body['MoveDate']).'</MoveDate>
      
      <DwellingType>'.strval($body['DwellingType']).'</DwellingType>
      
      <AmountOfFurnishings>M</AmountOfFurnishings>
      
      <PickupShuttle>N</PickupShuttle>
      
      <DeliveryShuttle>N</DeliveryShuttle>
      
      <AgentCode>2059000</AgentCode >
      
      </MoveDetails>
      
      <EstimateDetails>
      
      <HasVehicles>N</HasVehicles>
      
      <RequestedEstimateDate>3D</RequestedEstimateDate>
      
      <RequestedEstimateTimeOfDay>E</RequestedEstimateTimeOfDay>
      
      <Comments>
      Pick Up Date: '.strval($body['MoveDate']).',
      Delivery Date:'.strval($body['DeliveryDate']).',
			In-House Estimate Date: '.strval($body['InHouseDate']).',
			Home Type: '.strval($body['HomeType']).',
      Rooms In Home: 
      Living Room: '.strval($body['LivingRoom']).',
      Kitchen: '.strval($body['Kitchen']).',
      Dining Room: '.strval($body['DiningRoom']).',
      Office: '.strval($body['Office']).',
      Comments: '.strval($body['Comments']).'
      </Comments>
      
      </EstimateDetails>
      
      <HTTPReferrer></HTTPReferrer>
      
      <HTTPUserAgent></HTTPUserAgent>
      
      <VendorId></VendorId>
      
      <VendorName></VendorName>
      
      <VendorParms></VendorParms>
      
      <OptInForSpecialOffers></OptInForSpecialOffers>
      
      <ThirdPartyOptIn></ThirdPartyOptIn>
      
      <VendorDateTime></VendorDateTime>
      
      <ReferrerFirstName></ReferrerFirstName>
      
      <ReferrerLastName></ReferrerLastName> <ReferrerEmail></ReferrerEmail>
      
      <TCPAOptIn></TCPAOptIn>
      
      </EstimateRequest>';

      $curl = curl_init();
      
      curl_setopt_array($curl, array(
        CURLOPT_URL => "http://ballpark.allied.com/sirvaxmlfeed/requestanestimate.asmx/SendEstimateRequestXmlString",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $formatReturn,
        CURLOPT_HTTPHEADER => array(
          "content-type: application/x-www-form-urlencoded",
        ),
      ));
      
      $response = curl_exec($curl);
      $err = curl_error($curl);
      
      curl_close($curl);
      GFCommon::log_debug($formatReturn);
      if ($err) {
        GFCommon::log_debug( "cURL Error #:" . $err);
      } else {
        GFCommon::log_debug( $response );
      }
  }
?>